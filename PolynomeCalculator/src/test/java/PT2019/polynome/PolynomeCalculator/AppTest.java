package PT2019.polynome.PolynomeCalculator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import polynome.Polynome;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{	public Polynome n1,n2;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest()
    {
        //super( testName );
    }
    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    public void setUp() {
    	n1=new Polynome("6x^4+5x^3+x+14");
    	n2=new Polynome("3x^3+2x^2+x^1+9");}
    public void tearDown() {
    	n1=new Polynome("");
    	n2=new Polynome("");}
    public void testParse() {
    	String result=n1.toString();
    	assertTrue(result.compareTo("+6x^4+5x^3+1x^1+14x^0")==0);}
    public void testDegree() {
    	float result=n2.degree();
    	assertTrue(result==3);}
    public void testAdd() {
    	Polynome result=n1.add(n2);
    	Polynome t=new Polynome("6x^4+8x^3+2x^2+2x+23");
    	assertTrue(result.toString().compareTo(t.toString())==0);}
    public void testSubtract() {
    	Polynome result=n1.subtract(n2);
    	Polynome t=new Polynome("6x^4+2x^3-2x^2+5");
    	assertTrue(result.toString().compareTo(t.toString())==0);}
    public void testMultiply() {
    	Polynome result=n1.multiply(n2);
    	Polynome t=new Polynome("+18x^7+27x^6+16x^5+62x^4+89x^3+29x^2+23x+126");
    	assertTrue(result.toString().compareTo(t.toString())==0);}
    public void testDivide() {
    	String result=n1.divide(n2);
    	String t="Q: +2x^1+1x^0 R: -2x^3-4x^2-18x^1+5x^0";
    	assertTrue(result.toString().compareTo(t)==0);}
    public void testDerive() {
    	Polynome result=n2.derive();
    	Polynome t=new Polynome("+9x^2+4x^1+1x^0\r\n");
    	assertTrue(result.toString().compareTo(t.toString())==0);}
    public void testIntegrate() {
    	String result=n1.integrate();
    	assertTrue(result.toString().compareTo("+(6/5)x^5+(5/4)x^4+(1/2)x^2+(14/1)x^1")==0);}
}
