package PT2019.polynome.PolynomeCalculator;
import java.util.*;
import javax.swing.*;
import polynome.*;
import user_interface.UI;
/**
 * Hello world!
 *
 */
public class App extends UI
{
    public static void main( String[] args )
    {
    	SwingUtilities.invokeLater(new Runnable() {
    		public void run() {
    			new UI();
    		}
    	});
    }
}
