package polynome;

import java.util.*;

public class Polynome {
	private ArrayList<Monome> monomes;
	public Polynome(Polynome r) {
		this.monomes=new ArrayList<Monome>();
		this.monomes=r.monomes;
	}
	public Polynome() {
		this.monomes=new ArrayList<Monome>();
	}
	public Polynome(Monome o) {
		this.monomes=new ArrayList<Monome>();
		this.monomes.add(o);
	}
	public Polynome(String h) 
	{	this.monomes=new ArrayList<Monome>();
		String[] aux=h.split("(?=\\+)|(?=\\-)");
		for(String q:aux) {
			this.monomes.add(new Monome(q));
		}
		this.tidyUp();
	}
	/**
	   * @return String This method returns a String representing the polynome
	   */
	public String toString() {
		String k="";
		for(Monome q:monomes) {
			k=k+q.toString();
		}
		return k;
	}
	/**
	   * This method is used to arrange the polynomial
	   * in the order of the monomial's powers and
	   * erases the monomials with 0 coefficient
	   */
	private void tidyUp() {
		Collections.sort(monomes);
		for(int i=1;i<monomes.size();i++) {
			if(i>0&&monomes.get(i).getPower()==monomes.get(i-1).getPower()) {
				monomes.get(i).setCoefficient(monomes.get(i).getCoefficient()+monomes.get(i-1).getCoefficient());
				monomes.remove(i-1);
				i--;}}
	for(int i=0;i<monomes.size();i++) {
		if(i>=0)
			if(monomes.get(i).getCoefficient()==0) {
				monomes.remove(i);
				i--;}}}
	/**
	   * This method is used to add two polynomials. 
	   * @param k This is the Polynome to be added to this
	   * @return Polynome This returns sum of this Polynome and k
	   */
	public Polynome add(Polynome k) {
		Polynome result=new Polynome();
		for(Monome a:this.monomes) 
			result.monomes.add(a);
			for(Monome b:k.monomes) 
				result.monomes.add(b);
		result.tidyUp();
		return result;
	}
	/**
	   * This method is used to subtract a Polynome from another one
	   * @param k This is the Polynome to be subtracted from this
	   * @return Polynome This returns subtraction of k from this Polynome
	   */
	public Polynome subtract(Polynome k) {
		Polynome result=new Polynome();
		for(Monome a:this.monomes) 
			result.monomes.add(a);
			for(Monome b:k.monomes) {
				Monome b1=new Monome(-b.getCoefficient(),b.getPower());
				result.monomes.add(b1);}
		result.tidyUp();
		return result;
	}
	/**
	   * This method is used to multiply two polynomials. 
	   * @param k This is the Polynome to be multiplied with this
	   * @return Polynome This returns product of this Polynome and k
	   */
	public Polynome multiply(Polynome k) {
		Polynome result=new Polynome();
		for(Monome a:this.monomes)
			for(Monome b:k.monomes)
				result.monomes.add(a.multiply(b));
		result.tidyUp();
		return result;
	}
	/**
	   * This method is used to add two polynomials. 
	   * @return Polynome This returns the first derivative of this Polynome
	   */
	public Polynome derive() {
		Polynome result=new Polynome();
		for(Monome a:this.monomes) {
			result.monomes.add(a.derive());
		}
		result.tidyUp();
		return result;
	}
	/**
	   * This method is used to add two polynomials. 
	   * @return Polynome This returns the first integral of this Polynome
	   */
	public String integrate() {
		String result="";
		for(Monome a:this.monomes) {
			result+=a.integrate();
		}
		return result;
	}
	/**
	   * This method is used to divide two polynomials. 
	   * @param k This is the Polynome to be divided from this
	   * @return String This returns a string representing the quotient and remainder of the division
	   */
	public String divide(Polynome k) {
		if(k.monomes.isEmpty())
			return "Can't divide by null";
		else {
			Polynome quotient=new Polynome();
			Polynome remainder=new Polynome(this);
			while(!remainder.monomes.isEmpty()&&remainder.degree()>=k.degree()) {
				Monome t=remainder.monomes.get(0).divide(k.monomes.get(0));
				Polynome tp=new Polynome(t);
				quotient=quotient.add(tp);
				remainder=remainder.subtract(tp.multiply(k));
				quotient.tidyUp();
				remainder.tidyUp();
			}
		String result="Q: "+quotient.toString()+" R: "+remainder.toString();
		return result;
		}
	}
	/**
	   * This method is used to multiply two polynomials. 
	   * @return float This returns the degree of this polynomial
	   */
	public float degree() {
		float result=0;
		for(Monome a:this.monomes)
			if(a.getPower()>result&&a.getCoefficient()>0)
				result=a.getPower();
		return result;
	}
}