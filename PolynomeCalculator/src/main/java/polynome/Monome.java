package polynome;

public class Monome implements Comparable<Monome>{
	private float power;
	private float coefficient;
	public Monome() {
		this.power=0;
		this.coefficient=0;
	}
	public Monome(Monome o) {
		this.power=o.power;
		this.coefficient=o.coefficient;
	}
	public Monome(float coefficient,float power) {
		this.power=power;
		this.coefficient=coefficient;
	}
	public Monome(String mon) {
		String[] parts=mon.split("x");
		if(parts.length==0) {
			if(!mon.contains("x")) {
				this.coefficient=0;
				this.power=0;}
			else {
				this.coefficient=1;
				this.power=1;}
		}
		else {
			if(parts[0].length()>0&&parts[0].matches("(([+]|[-])[0-9]+)|([0-9]+)")) 
				this.coefficient=Integer.parseInt(parts[0]);
			else this.coefficient=1;
			if(parts.length==1) {
				if(mon.contains("x"))
					this.power=1;
				else
					this.power=0;}
			}
				if(parts.length>=2) {
					StringBuilder q=new StringBuilder(parts[1]);
					q.deleteCharAt(0);
					parts[1]=q.toString();
					if(parts[1].length()>0&&parts[1].matches("(([+]|[-])[0-9]+)|([0-9]+)"))
						this.power=Integer.parseInt(parts[1]);
					else
						this.power=0;
				}
	}
	/**
	   * This method is used to compare the powers of two Monomes and is used to order Monomes in a Polynome 
	   * @param o This is the Monome to be used at the comparison
	   * @return int 0 is returned if the Monomes are equal, -1 if this if bigger than o and 1 else
	   */
	public int compareTo(Monome o) {
		if(this.power==o.power)
			return 0;
		else if(this.power>o.power)
			return -1;
		else return 1;
	}
	public float getPower() {
		return power;
	}
	public void setPower(float power) {
		this.power = power;
	}
	public float getCoefficient() {
		return coefficient;
	}
	public void setCoefficient(float coefficient) {

		this.coefficient = coefficient;
	}
	/**
	   * This method is used to show the Monome in a String format 
	   * @return String This returns a string representing this Monome
	   */
	public String toString() {
		String c="";
		if(coefficient>0)
			c="+";
		else if(coefficient==0) return c;
		c=c.concat((int)coefficient+"x^"+(int)power);
		return c;
	}
	/**
	   * This method is used to add two Monomes. 
	   * @param o This is the Monome to be added to this
	   * @return Monome This returns the sum of this Monome and k
	   */
	public Monome add(Monome o) {
		Monome k;
		if(o.power==this.power) {
			k=new Monome(o.coefficient+this.coefficient,o.power);
			return k;}
		else {
			k=new Monome();
			return k;
	}
	}
	/**
	   * This method is used to subtract two Monomes. 
	   * @param o This is the Monome to be subtracted from this
	   * @return Monome This returns the subtraction of k from this
	   */
	public Monome subtract(Monome o) {
		Monome k;
		if(o.power==this.power) {
			k=new Monome(this.coefficient-o.coefficient,this.power);
			return k;
		}
		else {
			k=new Monome();
			return k;
		}
	}
	/**
	   * This method is used to multiply two Monomes. 
	   * @param o This is the Monome to be multiplied with this
	   * @return Monome This returns the product of this Monome and k
	   */
	public Monome multiply(Monome o) {
 		Monome result;
 		result=new Monome(this.coefficient*o.coefficient,this.power+o.power);
 		return result;
 	}
	/**
	   * This method is used to divide two Monomes. 
	   * @param o This is the Monome to be divided from this
	   * @return Monome This returns the division of k from Monome
	   */
	public Monome divide(Monome o) {
		Monome result;
		result=new Monome(this.coefficient/o.coefficient,this.power-o.power);
		return result;
	}
	/**
	   * This method is used to derive a Monome. 
	   * @return Monome This returns the Monome derived
	   */
	public Monome derive() {
 		Monome k=new Monome(this.coefficient*this.power,this.power-1);
 		return k;
 	}
	/**
	   * This method is used to integrate a Monome. 
	   * @return Monome This returns the Monome integrated
	   */
	public String integrate() {
		int powern=(int)this.power+1;
		return "+("+(int)this.coefficient+"/"+powern+")x^"+powern;
		
	}
	/**
	   * This method is used to check if a Monome has the coefficient 0. 
	   * @return boolean This returns true if the coefficient is 0, else it returns false
	   */
	public boolean isNull() {
		if(this.coefficient==0)
			return true;
		else
			return false;
	}
	/**
	   * This method is used to compute a Monome for a given value 
	   * @return float This returns the value computed
	   */
	public float compute(int x) {
		float result=1;
		for(int i=0;i<this.power;i++) {
			result*=x;
		}
		return result*this.coefficient;
	}
}
