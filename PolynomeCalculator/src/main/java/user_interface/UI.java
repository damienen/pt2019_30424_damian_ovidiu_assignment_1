package user_interface;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import polynome.Polynome;
public class UI {
	private JTextField textFieldPolynome1;
	private JTextField textFieldPolynome2;
	private JTextField textFieldResultPolynome;
	private class ButtonAddActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Polynome a=new Polynome(textFieldPolynome1.getText().trim());
			Polynome b=new Polynome(textFieldPolynome2.getText().trim());
			textFieldResultPolynome.setText(a.add(b).toString());
		}
		
	}
	private class ButtonSubtractActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Polynome a=new Polynome(textFieldPolynome1.getText().trim());
			Polynome b=new Polynome(textFieldPolynome2.getText().trim());
			textFieldResultPolynome.setText(a.subtract(b).toString());
		}
		
	}
	private class ButtonMultiplyActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Polynome a=new Polynome(textFieldPolynome1.getText().trim());
			Polynome b=new Polynome(textFieldPolynome2.getText().trim());
			textFieldResultPolynome.setText(a.multiply(b).toString());
		}
		
	}
	private class ButtonDerive1ActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Polynome a=new Polynome(textFieldPolynome1.getText().trim());
			//Polynome b=new Polynome(textFieldPolynome2.getText().trim());
			textFieldResultPolynome.setText(a.derive().toString());
		}
		
	}
	private class ButtonDerive2ActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			//Polynome a=new Polynome(textFieldPolynome1.getText().trim());
			Polynome b=new Polynome(textFieldPolynome2.getText().trim());
			textFieldResultPolynome.setText(b.derive().toString());
		}
		
	}
	private class ButtonIntegrate1ActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Polynome a=new Polynome(textFieldPolynome1.getText().trim());
			//Polynome b=new Polynome(textFieldPolynome2.getText().trim());
			textFieldResultPolynome.setText(a.integrate().toString());
		}
		
	}
	private class ButtonIntegrate2ActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			//Polynome a=new Polynome(textFieldPolynome1.getText().trim());
			Polynome b=new Polynome(textFieldPolynome2.getText().trim());
			textFieldResultPolynome.setText(b.integrate().toString());
		}
		
	}
	private class ButtonDivideActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Polynome a=new Polynome(textFieldPolynome1.getText().trim());
			Polynome b=new Polynome(textFieldPolynome2.getText().trim());
			textFieldResultPolynome.setText(a.divide(b).toString());
		}
		
	}
	public UI() {
		JFrame mainFrame=new JFrame("Polynome calculator");
		JPanel panel=new JPanel();
		//panel.setBackground(Color.cyan);
		mainFrame.getContentPane().add(panel);
		//POLYNOMES
		JLabel labelPolynome1=new JLabel("Polynome #1:");
		panel.add(labelPolynome1);
		textFieldPolynome1=new JTextField("6x^4+5x^3+x+14");
		textFieldPolynome1.setPreferredSize(new Dimension(200,20));
		panel.add(textFieldPolynome1);
		JLabel labelPolynome2=new JLabel("Polynome #2:");
		panel.add(labelPolynome2);
		textFieldPolynome2=new JTextField("3x^4+2x^3+x^2+9");
		textFieldPolynome2.setPreferredSize(new Dimension(200,20));
		panel.add(textFieldPolynome2);
		//BUTTONS
		JButton buttonAdd=new JButton("Add");
		buttonAdd.setPreferredSize(new Dimension(140,30));
		panel.add(buttonAdd);
		JButton buttonSubtract=new JButton("Subtract");
		buttonSubtract.setPreferredSize(new Dimension(140,30));
		panel.add(buttonSubtract);
		JButton buttonMultiply=new JButton("Multiply");
		buttonMultiply.setPreferredSize(new Dimension(140,30));
		panel.add(buttonMultiply);
		JButton buttonDivide=new JButton("Divide");
		buttonDivide.setPreferredSize(new Dimension(140,30));
		panel.add(buttonDivide);
		JButton buttonDerive1=new JButton("Derive #1");
		buttonDerive1.setPreferredSize(new Dimension(140,30));
		panel.add(buttonDerive1);
		JButton buttonIntegrate1=new JButton("Integrate #1");
		buttonIntegrate1.setPreferredSize(new Dimension(140,30));
		panel.add(buttonIntegrate1);
		JButton buttonDerive2=new JButton("Derive #2");
		buttonDerive2.setPreferredSize(new Dimension(140,30));
		panel.add(buttonDerive2);
		JButton buttonIntegrate2=new JButton("Integrate #2");
		buttonIntegrate2.setPreferredSize(new Dimension(140,30));
		panel.add(buttonIntegrate2);
		//RESULT
		JLabel labelResultPolynome=new JLabel("Result:");
		panel.add(labelResultPolynome);
		textFieldResultPolynome=new JTextField("9x^4+7x^3+x^2+1x^1+23x^0");
		textFieldResultPolynome.setPreferredSize(new Dimension(200,20));
		panel.add(textFieldResultPolynome);
		
		//Set size of the frame
		mainFrame.setPreferredSize(new Dimension(310,250));
		mainFrame.setSize(mainFrame.getPreferredSize());
		//Put the frame in the middle of the screen
		mainFrame.setLocationRelativeTo(null);
		//Close frame when clicking on X
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Make it of a stable size
		mainFrame.setResizable(false);
		//Make frame visible
		mainFrame.setVisible(true);
		
		//ACTION LISTENERS
		buttonAdd.addActionListener(new ButtonAddActionListener());
		buttonSubtract.addActionListener(new ButtonSubtractActionListener());
		buttonMultiply.addActionListener(new ButtonMultiplyActionListener());
		buttonDivide.addActionListener(new ButtonDivideActionListener());
		buttonDerive1.addActionListener(new ButtonDerive1ActionListener());
		buttonDerive2.addActionListener(new ButtonDerive2ActionListener());
		buttonIntegrate1.addActionListener(new ButtonIntegrate1ActionListener());
		buttonIntegrate2.addActionListener(new ButtonIntegrate2ActionListener());
	}

}
